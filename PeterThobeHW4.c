//Peter Thobe 
//HW4

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <GL/gl.h>
#include <GL/glut.h>
#define CANVAS_WIDTH 640
#define CANVAS_HEIGHT 480



struct shape 
{
    // denotes the color of the shape in integer values
    int red     ;
    int green   ;
    int blue    ;
    // Identifies if shape rectangle or Ellipse should be filled
    int filled  ;
    // Determines the type shape to be drawn
    int line    ;
    int bezier  ;   
    int rectangle     ;
    int ellipse     ;

    int x1, x2, x3, x4  ;
    int y1, y2, y3, y4  ;
};




// needed variables for this project
int maxShapes = 40  ;
struct shape draw[20]   ;
int numShapes = 0   ;
int red, green, blue    ;
int shapeDef[4] ;
int fill    ;
int mouseCounter = 0    ;
int colorCode   ;


// Draw shape Functions
 
 void drawLine(int i)
 {

        glBegin(GL_LINES)   ;
        glVertex2i(draw[i].x1, draw[i].y1)   ;
        glVertex2i(draw[i].x2, draw[i].y2)   ;
        glEnd() ;
        glFlush()   ;
 }

 void drawRectangle(int i)
 {
    if(draw[i].rectangle == 1)
    {
        if(draw[i].filled == 1)
        {
          glBegin(GL_POLYGON) ;
          glVertex2i(draw[i].x1, draw[i].y1) ;
          glVertex2i(draw[i].x2, draw[i].y1) ;
          glVertex2i(draw[i].x2, draw[i].y2) ;
          glVertex2i(draw[i].x1, draw[i].y2) ;
          glEnd() ;
        }
        else
        {
          glBegin(GL_LINE_LOOP) ;
          glVertex2i(draw[i].x1, draw[i].y1) ;
          glVertex2i(draw[i].x2, draw[i].y1) ;
          glVertex2i(draw[i].x2, draw[i].y2) ;
          glVertex2i(draw[i].x1, draw[i].y2) ;
          glEnd() ;
        }
        glFlush()   ;
    }
    else
    {
        printf("Error! This is not a rectangle shape!\n")   ;
    }
 }

 void drawBezier(int i)
 {
    
    float time = .05  ;
    float t = 0.0 ;
    float x,y   ;
    float BeginX,BeginY;
   
    glBegin(GL_LINES) ;
    for(t = 0.0 ; i <= 1.0 ; t = t + time)
    {
      x = (pow((1-t),3) * draw[i].x1) + ( 3 * pow((1-t),2) * t * draw[i].x2) + (3 * (1-t) * pow(t,2) * draw[i].x3)
                          + (pow(t,3) * draw[i].x4) ;

      y = (pow((1-t),3) * draw[i].y1) + ( 3 * pow((1-t),2) * t * draw[i].y2) + (3 * (1-t) * pow(t,2) * draw[i].y3)
                          + (pow(t,3) * draw[i].y4) ;

        if (t > 0)
        {
            glVertex2f(BeginX,BeginY)   ;
            glVertex2f(x,y) ;
        }
        BeginX = x  ;
        BeginY = y  ;
    }
    glEnd() ;
    glFlush() ;
 }


 void drawEllispe(int i)
 {
  float theta ;
  float majAxis , minAxis ;
  float x, y;
  int xMajAxis = 0;
  if(abs(draw[i].x1 - draw[i].x2)  >= abs(draw[i].y1 - draw[i].y2) )
  {
    xMajAxis = 1 ;
    majAxis = (draw[i].x1 - draw[i].x2)  ;
    minAxis = abs(draw[i].y1 - draw[i].y2) ;
  }
  else
  {
    majAxis = abs(draw[i].y1 - draw[i].y2)  ;
    minAxis = abs(draw[i].x1 - draw[i].x2) ;
  }
    if(draw[i].filled == 1)
    {
      glBegin(GL_POLYGON) ;
      for(theta  = 0 ; theta <= 2*M_PI ; theta = theta + M_PI/36)
      {
        if(xMajAxis == 1)
        {
            x = cos(theta) * majAxis /2  ;
            y = sin(theta) * minAxis /2  ;
        }
        else
        {
            x = cos(theta) * minAxis / 2    ;
            y - sin(theta) * majAxis / 2    ;
        }
        glVertex2f((draw[i].x1 + draw[i].x2) + x,(draw[i].y1 + draw[i].y2) + y) ;
      }
      glEnd() ;
    }
    else
    {
      glBegin(GL_LINE_LOOP) ;
      for(theta  = 0 ; theta <= 2*M_PI ; theta = theta + M_PI/36)
      {
        if(xMajAxis == 1)
        {
            x = cos(theta) * majAxis /2  ;
            y = sin(theta) * minAxis /2  ;
        }
        else
        {
            x = cos(theta) * minAxis / 2    ;
            y = sin(theta) * majAxis / 2    ;
        }
        glVertex2f((draw[i].x1 - draw[i].x2) + x,(draw[i].y1 - draw[i].y2) + y) ;
      }
      glEnd() ;
    }
    glFlush() ;
 }



/*
 display
*/
void display(void)
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();

    int i ;
    for(i = 0 ; i < numShapes ; i++)
    {
      glColor3f(draw[i].red,draw[i].green ,draw[i].blue );

        if(draw[i].line == 1)
        {
            drawLine(i) ;
        }
        else if(draw[i].rectangle == 1)
        {
            drawRectangle(i)    ;
        }
        else if(draw[i].ellipse == 1)
        {
            drawEllispe(i)  ;
        }
        else if(draw[i].bezier == 1)
        {
            drawBezier(i)   ;
        }
    }
  if(numShapes >= 20)
  {
    numShapes = 0 ; // resets numShapes to 0 one the maximum Number of shapes are drawn
  }
  glFlush() ;
}
/*
 reshape
*/
void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho (0.0, 640, 480, 0, -1.0, 1.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
/*
 Glut Post Redisplay method
*/
void spindisplay(void)
{
    glutPostRedisplay();
}
//------------------------------------------------------

// set shape type
void setLine()
{
    shapeDef[0] = 1 ;
    shapeDef[1] = 0 ;
    shapeDef[2] = 0 ;
    shapeDef[3] = 0 ;
}
void setRectangle()
{
    shapeDef[0] = 0 ;
    shapeDef[1] = 1 ;
    shapeDef[2] = 0 ;
    shapeDef[3] = 0 ;
}
void setEllipse()
{
    shapeDef[0] = 0 ;
    shapeDef[1] = 0 ;
    shapeDef[2] = 1 ;
    shapeDef[3] = 0 ;
}
void setBezier()
{
    shapeDef[0] = 0 ;
    shapeDef[1] = 0 ;
    shapeDef[2] = 0 ;
    shapeDef[3] = 1 ;
}

void setFilled()
{
    if(fill == 1)
    {
        draw[numShapes].filled = 1  ;
    printf("filled set");
    }
    else 
    {
        draw[numShapes].filled = 0  ;
    }
}

void setColor(int i)
{
    switch (i)
    {
    printf("Color Menu\n");
        case 0://red
            draw[numShapes].red = 255   ;
            draw[numShapes].green = 0   ;
            draw[numShapes].blue = 0    ;
            break   ;
        case 1://green
            draw[numShapes].red = 0 ;
            draw[numShapes].green = 255 ;
            draw[numShapes].blue = 0    ;
            break   ;
        case 2://blue
            draw[numShapes].red = 0 ;
            draw[numShapes].green = 0   ;
            draw[numShapes].blue =  255 ;
            break   ;
        case 3://orange
            draw[numShapes].red = 255  ;
            draw[numShapes].green = 128;
            draw[numShapes].blue = 0  ;
            break   ;
        
        case 4://yellow
            draw[numShapes].red = 255   ;
            draw[numShapes].green = 255 ;
            draw[numShapes].blue = 0  ;
            break   ;
        case 5://white
            draw[numShapes].red = 255   ;
            draw[numShapes].green = 255 ;
            draw[numShapes].blue = 255  ;
            break   ;
        case 6://cyan
            draw[numShapes].red = 0 ;
            draw[numShapes].green = 255 ;
            draw[numShapes].blue = 255  ;
            break   ;
        case 7://purple
            draw[numShapes].red = 100   ;
            draw[numShapes].green = 0  ;
            draw[numShapes].blue = 255  ;
            break   ;
      default:
         break  ;
     }
    glutPostRedisplay();
}

void mouse(int mouse_button, int state, int x, int y)
{
    if(mouse_button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
    {
    
        // if(Line or Rectangle or Ellipse store 2 points)
        if(shapeDef[0] == 1 || shapeDef[1] == 1 || shapeDef[2] == 1)
        {
      if(mouseCounter > 1)
      {
        mouseCounter = 0  ;
      }
            if(mouseCounter == 0)
            {
                printf("point 1 initialized\n") ;
                //Point 1 assigned to point object
                draw[numShapes].x1 = x;
                draw[numShapes].y1 = y;
                mouseCounter++  ;
                printf("x1: %d, y1: %d\n", x,y)   ;
            }
            else if(mouseCounter == 1)
            {
                 printf("point 2 initialized \n")  ;
                // Point 2 assigned to shape object
                draw[numShapes].x2 = x;
                draw[numShapes].y2 = x;
                printf("x2: %d, y2: %d\n", x,y)   ;
                if(shapeDef[0] == 1)
                {
                    draw[numShapes].line = 1    ;
                }
                else if(shapeDef[1] == 1)
                {
                    draw[numShapes].rectangle = 1   ;
          draw[numShapes].filled = fill   ;
                }
                else if(shapeDef[2] == 1)
                {
                    draw[numShapes].ellipse = 1 ;
          draw[numShapes].filled = fill   ;
                }

        mouseCounter = 0  ; // reset mouse counter
                // Sets color (rgb values) for shape obj
                setColor(colorCode) ;

                //Increment Num shapes
                numShapes++ ;
            }
        }
        else if(shapeDef[3] == 1)
        {
            if(mouseCounter == 0)
            {
        printf("point 1 initialized \n")  ;
                draw[numShapes].x1 = x  ;
                draw[numShapes].y1 = y  ;
                mouseCounter++  ;

            }
            else if(mouseCounter == 1)
            {
        printf("point 2 initialized \n")  ;
                draw[numShapes].x2 = x  ;
                draw[numShapes].y2 = y  ;
                mouseCounter++  ;

            }
            else if(mouseCounter == 2)
            {
        printf("point 3 initialized \n")  ;
                draw[numShapes].x3 = x  ;
                draw[numShapes].y3 = y  ;
                mouseCounter++  ;
            }
            else if(mouseCounter == 3)
            {
        printf("point 4 initialized \n")  ;
                draw[numShapes].x4 = x  ;
                draw[numShapes].y4 = y  ;
                mouseCounter = 0    ;
                // increment numShapes
                if(shapeDef[3] == 1)
                {
                    draw[numShapes].bezier = 1  ;
                }

             setColor(colorCode)    ;
             numShapes++    ;
            }
        }       
        glutPostRedisplay() ;
    }
}
void menu (int value)
{
  switch (value) 
  {
    case 1:
        setLine()   ;
        colorCode = 0   ;
        break;
    case 2:
        setLine()   ;
      colorCode = 1 ;
      break;
    case 3:
        setLine()   ;
        colorCode = 2   ;
        break;
    case 4:
        setLine()   ;
        colorCode = 3   ;
    break;
    case 5:
        setLine()   ;
        colorCode = 4   ;
        break;
    case 6:
        setLine()   ;
        colorCode = 5   ;
      break;
    case 7:
        setLine()   ;
        colorCode = 6   ;
        break;
    case 8:
        setLine()   ;
        colorCode = 7   ;
        break;
    case 9:
      setBezier()   ;
      colorCode = 0 ;
    break;
    case 10:
      setBezier()   ;
      colorCode = 1 ;
    break;
    case 11:
      setBezier()   ;
      colorCode = 2 ;
    break;
    case 12:
      setBezier()   ;
      colorCode = 3 ;

    break;
    case 13:
      setBezier()   ;
      colorCode = 4 ;
    break;
    case 14:
      setBezier()   ;
      colorCode = 5 ;
    break;
    case 15:
      setBezier()   ;
      colorCode = 6 ;
    break;
    case 16:
      setBezier()   ;
      colorCode = 7 ;
    break;
    case 17:
    setRectangle()  ;
    colorCode = 0   ;
    fill = 0    ;
    break;
    case 18:
    setRectangle()  ;
    colorCode = 1   ;
    fill = 0    ;
    break;
    case 19:
    setRectangle()  ;
    colorCode = 2   ;
    fill = 0    ;
    break;
    case 20:
    setRectangle()  ;
    colorCode = 3   ;
    fill = 0    ; 
    break;
    case 21:
    setRectangle()  ;
    colorCode = 4   ;
    fill = 0    ;
    break;
    case 22:
    setRectangle()  ;
    colorCode = 5   ;
    fill = 0    ;
    break;
    case 23:
    setRectangle()  ;
    colorCode = 6   ;
    fill = 0    ;
    break;
    case 24:
    setRectangle()  ;
    colorCode = 7   ;
    fill = 0    ;
    break;
    case 25:
    setRectangle()  ;
    colorCode = 0   ;
    fill = 1    ;
    break;
    case 26:
    setRectangle()  ;
    colorCode = 1   ;
    fill = 1    ;
    break;
    case 27:
    setRectangle()  ;
    colorCode = 2   ;
    fill = 1    ;  
    break;
    case 28:
    setRectangle()  ;
    colorCode = 3   ;
    fill = 1    ;  
    break;
    case 29:
    setRectangle()  ;
    colorCode = 4   ;
    fill = 1    ;  
    break;
    case 30:
    setRectangle()  ;
    colorCode = 5   ;
    fill = 1    ;  
    break;
    case 31:
    setRectangle()  ;
    colorCode = 6   ;
    fill = 1    ;  
    break;
    case 32:
    setRectangle()  ;
    colorCode = 7   ;
    fill = 1    ;  
    break;
    case 33:
    setEllipse()    ;
    colorCode = 0   ;
    fill = 1    ;
    break;
    case 34:
    setEllipse()    ;
    colorCode = 1   ;
    fill = 0    ;
    break;
    case 35:
    setEllipse()    ;
    colorCode = 2   ;
    fill = 0    ;  
    break;
    case 36:
    setEllipse()    ;
    colorCode = 3   ;
    fill = 0    ;  
    break;
    case 37:
    setEllipse()    ;
    colorCode = 4   ;
    fill = 0    ;  
    break;
    case 38:
    setEllipse()    ;
    colorCode = 5   ;
    fill = 0    ;  
    break;
    case 39:
    setEllipse()    ;
    colorCode = 6   ;
    fill = 0    ;  
    break;
    case 40:
    setEllipse()    ;
    colorCode = 7   ;
    fill = 0    ;  
    break;
    case 41:
    setEllipse()    ;
    colorCode = 0   ;
    fill = 0    ;  
    break;
    case 42:
    setEllipse()    ;
    colorCode = 1   ;
    fill = 1    ;  
    break;
    case 43:
    setEllipse()    ;
    colorCode = 2   ;
    fill = 1    ;  
    break;
    case 44:
    setEllipse()    ;
    colorCode = 3   ;
    fill = 1    ;  
    break;
    case 45:
    setEllipse()    ;
    colorCode = 4   ;
    fill = 1    ;  
    break;
    case 46:
    setEllipse()    ;
    colorCode = 5   ;
    fill = 1    ;  
    break;
    case 47:
    setEllipse()    ;
    colorCode = 6   ;
    fill = 1    ;  
    break;
    case 48:
    setEllipse()    ;
    colorCode = 7   ;
    fill = 1    ;  
    break;

    // Default 
    default: 
    break;
  }

  glutPostRedisplay();
}




int main(int argc, char *argv[])
{
    int glut_sub_menu, glut_sub_sub_menu;
    int lineColorMenu , bezierColorMenu, rectangleFilledMenu, rectangleColorMenu1, 
        rectangleColorMenu2, ellipseFilledMenu , ellipseColorMenu1, ellipseColorMenu2 ; 

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(CANVAS_WIDTH, CANVAS_HEIGHT);
    glutInitWindowPosition (0,0);    
    glutCreateWindow ("HW4");
    //Menu Start

    lineColorMenu = glutCreateMenu (menu)   ;
  glutAddMenuEntry ("RED", 1);
  glutAddMenuEntry ("GREEN", 2);
  glutAddMenuEntry ("BLUE", 3);
  glutAddMenuEntry ("ORANGE", 4);
  glutAddMenuEntry ("YELLOW", 5);
  glutAddMenuEntry ("WHITE", 6);
  glutAddMenuEntry ("CYAN", 7);
  glutAddMenuEntry ("PURPLE", 8);

  bezierColorMenu = glutCreateMenu (menu)   ;
  glutAddMenuEntry ("RED", 9);
  glutAddMenuEntry ("GREEN", 10);
  glutAddMenuEntry ("BLUE", 11);
  glutAddMenuEntry ("ORANGE", 12);
  glutAddMenuEntry ("YELLOW", 13);
  glutAddMenuEntry ("WHITE", 14);
  glutAddMenuEntry ("CYAN", 15);
  glutAddMenuEntry ("PURPLE", 16);

  rectangleColorMenu1 = glutCreateMenu (menu)   ;
  glutAddMenuEntry ("RED", 17);
  glutAddMenuEntry ("GREEN", 18);
  glutAddMenuEntry ("BLUE", 19);
  glutAddMenuEntry ("ORANGE", 20);
  glutAddMenuEntry ("YELLOW", 21);
  glutAddMenuEntry ("WHITE", 22);
  glutAddMenuEntry ("CYAN", 23);
  glutAddMenuEntry ("PURPLE", 24);

  rectangleColorMenu2 = glutCreateMenu (menu)   ;  //filled
  glutAddMenuEntry ("RED", 25);
  glutAddMenuEntry ("GREEN", 26);
  glutAddMenuEntry ("BLUE", 27);
  glutAddMenuEntry ("ORANGE", 28);
  glutAddMenuEntry ("YELLOW", 29);
  glutAddMenuEntry ("WHITE", 30);
  glutAddMenuEntry ("CYAN", 31);
  glutAddMenuEntry ("PURPLE", 32);
  //glutCreateMenu (menu) ;

  ellipseColorMenu1 = glutCreateMenu (menu) ;
  glutAddMenuEntry ("RED", 33);
  glutAddMenuEntry ("GREEN", 34);
  glutAddMenuEntry ("BLUE", 35);
  glutAddMenuEntry ("ORANGE", 36);
  glutAddMenuEntry ("YELLOW", 37);
  glutAddMenuEntry ("WHITE", 38);
  glutAddMenuEntry ("CYAN", 39);
  glutAddMenuEntry ("PURPLE", 40);
  //glutCreateMenu (menu) ;

  ellipseColorMenu2 = glutCreateMenu (menu) ; // filled
  glutAddMenuEntry ("RED", 41);
  glutAddMenuEntry ("GREEN", 42);
  glutAddMenuEntry ("BLUE", 43);
  glutAddMenuEntry ("ORANGE", 44);
  glutAddMenuEntry ("YELLOW", 45);
  glutAddMenuEntry ("WHITE", 46);
  glutAddMenuEntry ("CYAN", 47);
  glutAddMenuEntry ("PURPLE", 48);
  //glutCreateMenu (menu) ;

  rectangleFilledMenu = glutCreateMenu(menu)    ;
  glutAddSubMenu ("UNFILLED", rectangleColorMenu1)  ;
  glutAddSubMenu ("FILLED", rectangleColorMenu2)    ;

  ellipseFilledMenu = glutCreateMenu(menu)  ;
  glutAddSubMenu ("UNFILLED", ellipseColorMenu1)    ;
  glutAddSubMenu ("FILLED", ellipseColorMenu2)  ;

  glutCreateMenu (menu);
  glutAddSubMenu ("LINE", lineColorMenu);   // line selected(shapeDef0)
  glutAddSubMenu ("BEZIER",bezierColorMenu) ;   // Bezier Selected(shapeDef3)
  glutAddSubMenu ("RECTANGLE",rectangleFilledMenu)  ;   // Rectangle Selected(ShapeDef1)
  glutAddSubMenu ("ELLIPSE", ellipseFilledMenu) ;   // Ellipse Selected (ShapeDef2)

  glutAttachMenu (GLUT_LEFT_BUTTON);
//Menu End
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
   
    glutMouseFunc(mouse)    ;
    
    glutMainLoop();
}