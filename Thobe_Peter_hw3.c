// Peter Thobe

#ifdef __APPLE__
  #include <OpenGL/gl.h>
  #include <OpenGL/glu.h>
  #include <GLUT/glut.h>
#elif __linux__
  #include <GL/glut.h>
#endif
#include <math.h>
#include <stdio.h>
#include <stdbool.h>


// Variables
float camera_angle_degrees = 0.0f;
float camera_position_x = 200.0f, camera_position_y = 000.0f, camera_position_z = 000.0f;

float center_x = 100.0f,center_y = 00.0f,center_z = 0.0f;

int maxX = 100, maxY= 25, maxZ = 50 ;

bool solid = false  ;

struct Vertex 
{
float x;
float y;
float z;
};

struct Face 
{
int vertex1 ;
int vertex2 ;
int vertex3 ;
};

// Counter for the Number of Vertices and for Face described by the passed obj file
int vertexCounter = 0 ;
int faceCounter = 0 ;

struct Vertex vertices[10000];
struct Face faces[10000]	;
 


//Initialize display function
void init (void)
{
  glShadeModel (GL_SMOOTH);
  glClearColor (0.0f, 0.0f, 0.0f, 0.0f);        
  glClearDepth (1.0f);
  glEnable (GL_DEPTH_TEST);
  glDepthFunc (GL_LEQUAL);
  glEnable (GL_COLOR_MATERIAL);
  glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
  glEnable (GL_LIGHTING);
  glEnable (GL_LIGHT0);
  GLfloat lightPos[4] = {-1.0, 1.0, 0.5, 0.0};
  glLightfv (GL_LIGHT0, GL_POSITION, (GLfloat *) &lightPos);
  glEnable (GL_LIGHT1);
  GLfloat lightAmbient1[4] = {0.0, 0.0,  0.0, 0.0};
  GLfloat lightPos1[4]     = {1.0, 0.0, -0.2, 0.0};
  GLfloat lightDiffuse1[4] = {0.5, 0.5,  0.3, 0.0};
  glLightfv (GL_LIGHT1,GL_POSITION, (GLfloat *) &lightPos1);
  glLightfv (GL_LIGHT1,GL_AMBIENT, (GLfloat *) &lightAmbient1);
  glLightfv (GL_LIGHT1,GL_DIFFUSE, (GLfloat *) &lightDiffuse1);
  glLightModeli (GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
}


// Main Display Function

void display (void)
{
   center_x = 0.0f; 
   center_y = 0.0f;
   center_z = 0.0f;

    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity ();
    gluLookAt (camera_position_x, camera_position_y, camera_position_z, center_x, center_y, center_z, 0.0f, 1.0f, 0.0f);
    // will need to fix the above example
  glPushMatrix();


  //call draw function for the rabbit

  if (solid == true)
  {
    glPolygonMode (GL_FRONT, GL_FILL);
  }
  else
  {
    glPolygonMode (GL_FRONT, GL_LINE);
  }
  int i ;
  for(i = 0 ; i <= faceCounter ; i++)
  {
  	glBegin(GL_TRIANGLES)	;
  		glVertex3f(vertices[ faces[i].vertex1 ].x, vertices[ faces[i].vertex1 ].y, vertices[ faces[i].vertex1 ].z)	;
  		glVertex3f(vertices[ faces[i].vertex2 ].x, vertices[ faces[i].vertex2 ].y, vertices[ faces[i].vertex2 ].z)	;
  		glVertex3f(vertices[ faces[i].vertex3 ].x, vertices[ faces[i].vertex3 ].y, vertices[ faces[i].vertex3 ].z)	;
  	glEnd();
  }

  // Originally inside the Program
  glPopMatrix();
  glFlush();

  glutSwapBuffers();
  glutPostRedisplay();
}




// reshape function from bruces example
void reshape (int w, int h)  
{
  glViewport (0, 0, w, h);
  glMatrixMode (GL_PROJECTION); 
  glLoadIdentity ();  
  if (h == 0)  
  { 
    gluPerspective (80, (float) w, 1.0, 5000.0);
  }
  else
  {
    gluPerspective (80, (float) w / (float) h, 1.0, 5000.0);
  }
  glMatrixMode (GL_MODELVIEW);  
  glLoadIdentity (); 
}



/*   ******Need to fix arrows to control the camera away from the model  */
// left arrow should rotate counter clockwise, right arrow will rotate clock wise
void arrow_keys(int key, int x, int y)
{
  switch(key)
  {
    case GLUT_KEY_UP: //left
    {
      if(camera_position_x > - 10) { camera_position_x--;center_x--;}
      break;
    }
    case GLUT_KEY_DOWN://right
    {
      if(camera_position_x < maxX + 10) { camera_position_x++;center_x++;}
      break;
    }
    case GLUT_KEY_LEFT://up
    {
      if(camera_position_z < maxZ + 1.5){ camera_position_z++; center_z++;}
      
      break;
    }
    case GLUT_KEY_RIGHT://down
    {
      if(camera_position_z > -maxZ) {camera_position_z--; center_z--;}
      
      break;
    }
    
  }

}

// Keyboard Actions to Take/ Listener w = wireframe/s = solid body object
void keyboard (unsigned char key, int x, int y)
{
  switch (key)
  {
    case 'w': // Wire Frame Display button
      if(solid == true)
      {
        solid = false ;
      }
      break;
  case 's': //Displays this as a Solid Body Model
      if (solid == false)
      {
        solid = true  ;
      }
    break;

  case 'e': //changes the color of the rabbit to red
    glColor3ub(255,0,0) ;
    break;
  case 'r': //changes the color of the rabbit to blue
    glColor3ub(0,0,255) ;
    break;
  case 't': //changes the color of the rabbit to green
    glColor3ub(0,255,0) ;
    break;
  case 'y': //changes the color of the rabbit to white
    glColor3ub(255,255,255) ;
    break;
  case 'u': //changes the color of the rabbit to orange
    glColor3ub(255,165,0) ;
    break;

  }
}

void readFile(char *fileLoc)
{
  FILE *file  ;
  int readFile	;
  float x,y,z  ;
  char ch 	; 

  // int numStart and numEnd
  int numStart  ;
  int numEnd  ;
  
  // 
  file = fopen(fileLoc,"r")  ;
  if(!file)
  {
    printf("file: %s, could not be accessed.\n", fileLoc)  ;
    exit(1) ;
  }
  glPointSize(1.0)	;
  while(!(feof(file)))
  {
  	readFile = fscanf(file, "%c %f %f %f", &ch, &x, &y, &z)	;
  	if(readFile == 4 && ch == 'v')
  	{
  		if(x < 3 || y < 3 || z < 3)
  		{
	  		vertices[vertexCounter].x = x * 1000	;
	  		vertices[vertexCounter].y = y * 1000	;
	  		vertices[vertexCounter].z = z * 1000	;
	  		vertexCounter++	;
	  	}
	  	else 
	  	{
	  		vertices[vertexCounter].x = x 	;
	  		vertices[vertexCounter].y = y 	;
	  		vertices[vertexCounter].z = z 	;
	  		vertexCounter++	;
	  	}
  	}
  	else if(readFile == 4 && ch == 'f')
  	{
  		faces[faceCounter].vertex1 = (int)x - 1	;
  		faces[faceCounter].vertex2 = (int)y - 1	;
  		faces[faceCounter].vertex3 = (int)z - 1	;
  		faceCounter++;
  	}
  }

}



int main (int argc, char *argv[]) 
{
  if (argc == 2)
  {
    readFile(argv[1])  ;
  }
  else
  {
    if(argc > 2)
    {
      printf("Too Many arguments given")  ;
      exit(1) ;
    }
    else
    {
      printf("No display information given!") ;
      exit(1) ;
    }
  }


  //Standard GLUT operations for Main Function
  int window;
  glColor3ub(0,0,255) ;
  glutInit (&argc, argv);
  glutInitDisplayMode (GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);
  glutInitWindowSize (1920, 1080 ); 
  glutInitWindowPosition (0, 0);
  window = glutCreateWindow ("Peter Thobe Homework #3");
  init();   // Initialize all the display parameters
  glutDisplayFunc (display); // handles lighting for the rabbit.
  glutReshapeFunc (reshape);//handles resize of window
  glutKeyboardFunc (keyboard);//handle keyboard input
  glutSpecialFunc (arrow_keys);//arrow key input
  glutMainLoop (); //important part
}
